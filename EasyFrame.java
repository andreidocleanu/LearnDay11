package LearnDay11;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class EasyFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	JButton buttonCancel = new JButton("Cancel");
	JButton buttonExit = new JButton("Exit");
	JLabel labelOne = new JLabel("O eticheta");
	JTextField textFieldOne = new JTextField("Text field cu text in el");
	JCheckBox checkBold = new JCheckBox("Bold");
	JCheckBox checkItalic = new JCheckBox("Italic");
	JRadioButton radioRed = new JRadioButton("Red");
	JRadioButton radioBlue = new JRadioButton("Blue");
	JComboBox<String> comboBox = new JComboBox<String>(new String[] { "Freshman", "Sophomore", "Junior", "Senior" });

	public EasyFrame(String title) {

		setTitle(title);
		setSize(450, 250);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(200, 100);
		setContentPane(getContentPane());
	}

	public JPanel getContentPane() {

		JPanel panel = new JPanel();
		JPanel panelOne = new JPanel();
		JPanel southPanel = new JPanel();
		JPanel northPanel = new JPanel();

		panel.setLayout(new BorderLayout());

		northPanel.add(labelOne);

		southPanel.add(buttonCancel);
		southPanel.add(buttonExit);

		panelOne.add(textFieldOne);
		panelOne.add(checkBold);
		panelOne.add(checkItalic);
		panelOne.add(radioRed);
		panelOne.add(radioBlue);
		panelOne.add(comboBox);

		panel.add(northPanel, BorderLayout.NORTH);
		panel.add(panelOne, BorderLayout.CENTER);
		panel.add(southPanel, BorderLayout.SOUTH);

		return panel;

	}

}
